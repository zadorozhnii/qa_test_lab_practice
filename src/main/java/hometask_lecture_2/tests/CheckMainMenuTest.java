package hometask_lecture_2.tests;

import hometask_lecture_2.BaseScript;
import hometask_lecture_2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class CheckMainMenuTest extends BaseScript {

    private static By pageTitle = By.tagName("h2");
    private static String sideMenuItem = "//li[contains(@class,'link-levelone') or contains(@class,'maintab')]";

    public static void main(String[] args) {
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.get(Properties.getBaseAdminUrl());
        loginAdminPanel(driver);
        List<WebElement> menuItem = driver.findElements(By.xpath(sideMenuItem));
        actWithMenuItems(menuItem, driver);
        System.out.println("<<< Script has finished successfully >>>");
        logOutAdminPanel(driver);
    }

    public static void actWithMenuItems(List<WebElement> list, WebDriver driver) {
        for (int x = 1; x < list.size() + 1; x++) {
            driver.findElement(By.xpath(sideMenuItem + "[" + x + "]")).click();
            waitFor(2000);
            String pageTitleBeforeRefresh = driver.findElement(pageTitle).getText();
            System.out.println(pageTitleBeforeRefresh);
            driver.navigate().refresh();
            waitFor(2000);
            String pageTitleAfterRefresh = driver.findElement(pageTitle).getText();
            System.out.println("Is user on the same page " + (pageTitleBeforeRefresh.equals(pageTitleAfterRefresh)));
        }
    }
}



