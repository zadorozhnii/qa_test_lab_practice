package hometask_lecture_2.tests;

import hometask_lecture_2.BaseScript;
import hometask_lecture_2.utils.Properties;
import org.openqa.selenium.WebDriver;

public class LoginTest extends BaseScript {

    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.get(Properties.getBaseAdminUrl());
        loginAdminPanel(driver);
        System.out.println("<<< Script has finished successfully >>>");
        logOutAdminPanel(driver);
    }
}

