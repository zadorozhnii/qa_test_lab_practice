package hometask_lecture_2;

import hometask_lecture_2.utils.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public abstract class BaseScript {

    private static WebDriver driver = null;

    private static By loginEmail = By.id("email");
    private static By password = By.id("passwd");
    private static By submitButton = By.name("submitLogin");
    private static By menuIcon = By.cssSelector(".imgm.img-thumbnail");
    private static By logoutButton = By.id("header_logout");

    /**
     *
     * @return instance of {@link WebDriver} object.
     */
    public static WebDriver getDriver() {
        if (driver == null){
            setWebDriver();
            driver = new ChromeDriver();
        }

        return driver;
    }

    /**
     *
     * Set WebDriver properties.
     */
    public static void setWebDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/chromedriver.exe");
    }

    /**
     *
     * Method logins to admin panel.
     */
    public static void loginAdminPanel(WebDriver driver) {
        driver.findElement(loginEmail).sendKeys(Properties.getLogin());
        driver.findElement(password).sendKeys(Properties.getPassword());
        driver.findElement(submitButton).click();
        waitFor(2000);
    }

    /**
     *
     * Method logouts to admin panel.
     */
    public static void logOutAdminPanel(WebDriver driver) {
        System.out.println("<<< Logging out >>>");
        driver.findElement(menuIcon).click();
        driver.findElement(logoutButton).click();
        driver.quit();
    }

    /**
     *
     * Method waits for particular time period.
     */
    public static void waitFor(int x){
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}